I recommend setting the following environment variables:

```
export LESS=-FRSX
export PAGER=less
```
