// Copyright 2018-2019 Matt Kraai

// This file is part of odo.

// odo is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.

// odo is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.

// You should have received a copy of the GNU Affero General Public License
// along with odo.  If not, see <https://www.gnu.org/licenses/>.

use chrono::{DateTime, Local};
use directories::ProjectDirs;
#[cfg(unix)]
use pager::Pager;
use regex::Regex;
use serde_derive::{Deserialize, Serialize};
use std::{collections::BTreeMap, fs::{self, File, OpenOptions}, io::{BufRead, BufReader, Write}, path::PathBuf, process};
use structopt::StructOpt;
use uuid::Uuid;

#[derive(StructOpt)]
struct Config {
    #[structopt(subcommand)]
    subcommand: Option<SubCommand>,
}

#[derive(StructOpt)]
enum SubCommand {
    #[structopt(name = "add")]
    Add {
        /// Mark the task done
        #[structopt(long = "done")]
        done: bool,
        /// Suppress all normal output
        #[structopt(short = "q", long = "quiet")]
        quiet: bool,
        /// Only add tasks that don't already exist
        #[structopt(short = "u", long = "unique")]
        unique: bool,
        #[structopt(raw(required = "true"))]
        description: Vec<String>,
    },
    #[structopt(name = "count")]
    Count {},
    #[structopt(name = "do")]
    Do {
        #[structopt(raw(required = "true"))]
        uuids: Vec<Uuid>,
    },
    #[structopt(name = "edit")]
    Edit {
        uuid: Uuid,
        #[structopt(raw(required = "true"))]
        description: Vec<String>,
    },
    #[structopt(name = "grep")]
    Grep {
        /// Ignore case distinctions, so that characters that differ only in case match each other
        #[structopt(short = "i", long = "ignore-case")]
        ignore_case: bool,
        pattern: String,
    },
    #[structopt(name = "log")]
    Log {},
    #[structopt(name = "rm")]
    Rm {
        #[structopt(raw(required = "true"))]
        uuids: Vec<Uuid>,
    },
}

#[derive(Deserialize, Serialize)]
#[serde(tag = "command", rename_all = "lowercase")]
enum Event {
    Add { timestamp: DateTime<Local>, uuid: Uuid, description: String },
    Do { timestamp: DateTime<Local>, uuid: Uuid },
    Edit { timestamp: DateTime<Local>, uuid: Uuid, description: String },
    Remove { timestamp: DateTime<Local>, uuid: Uuid },
}

fn main() {
    if let Err(e) = run() {
        eprintln!("odo: {}", e);
        process::exit(1);
    }
}

fn run() -> Result<(), String> {
    let config = Config::from_args();
    match config.subcommand {
        Some(subcommand) => match subcommand {
            SubCommand::Add { done, quiet, unique, description } => {
                let description = description.join(" ");
                if unique {
                    for Task { description: existing, .. } in read_tasks()? {
                        if description == existing {
                            return Ok(());
                        }
                    }
                }
                let uuid = Uuid::new_v4();
                let timestamp = Local::now();
                log_event(&Event::Add { uuid, description, timestamp })?;
                if !quiet {
                    println!("Added {}.", uuid);
                }
                if done {
                    log_event(&Event::Do { timestamp, uuid })?;
                }
            }
            SubCommand::Count {} => {
                println!("{}", read_tasks()?.len());
            }
            SubCommand::Do { uuids } => {
                for uuid in uuids {
                    let timestamp = Local::now();
                    log_event(&Event::Do { timestamp, uuid })?;
                }
            }
            SubCommand::Edit { uuid, description } => {
                let description = description.join(" ");
                let timestamp = Local::now();
                log_event(&Event::Edit { timestamp, uuid, description })?;
            }
            SubCommand::Grep { ignore_case, pattern } => {
                set_up_pager();
                let pattern = if ignore_case {
                    format!("(?i){}", pattern)
                } else {
                    pattern
                };
                let re = Regex::new(&pattern).map_err(|e| format!("invalid pattern: {}", e))?;
                for Task { uuid, description } in read_tasks()? {
                    if re.is_match(&description) {
                        println!("{}: {}", uuid, description);
                    }
                }
            }
            SubCommand::Log {} => {
                set_up_pager();
                let mut map = BTreeMap::new();
                for event in read_events()? {
                    match event? {
                        Event::Add { uuid, description, .. } => {
                            map.insert(uuid, description);
                        }
                        Event::Do { uuid, timestamp } => {
                            println!("{} {}", timestamp.format("%FT%T"), map[&uuid]);
                            map.remove(&uuid);
                        }
                        Event::Edit { uuid, description, .. } => {
                            map.insert(uuid, description);
                        }
                        Event::Remove { uuid, .. } => {
                            map.remove(&uuid);
                        }
                    }
                }
            }
            SubCommand::Rm { uuids } => {
                for uuid in uuids {
                    let timestamp = Local::now();
                    log_event(&Event::Remove { timestamp, uuid })?;
                }
            }
        }
        None => {
            set_up_pager();
            for Task { uuid, description } in read_tasks()? {
                println!("{}: {}", uuid, description);
            }
        }
    }
    Ok(())
}

fn set_up_pager() {
    #[cfg(unix)]
    Pager::new().setup();
}

fn log_event(event: &Event) -> Result<(), String> {
    let project_dirs = ProjectDirs::from("org", "ftbfs", "odo").ok_or_else(|| "unable to determine project directories".to_string())?;
    let data_dir = project_dirs.data_dir();
    fs::create_dir_all(&data_dir).map_err(|e| format!("unable to create {}: {}", data_dir.display(), e))?;
    let path = data_dir.join("log");
    let mut file = OpenOptions::new().create(true).append(true).open(&path).map_err(|e| format!("unable to open {}: {}", path.display(), e))?;
    let event = serde_json::to_string(event).map_err(|e| format!("unable to serialize command: {}", e))?;
    writeln!(file, "{}", event).map_err(|e| format!("unable to write to {}: {}", path.display(), e))?;
    Ok(())
}

struct Events {
    path: PathBuf,
    line_number: usize,
    file: Option<BufReader<File>>,
}

impl Iterator for Events {
    type Item = Result<Event, String>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.file {
            Some(ref mut file) => {
                let mut line = String::new();
                if let Err(e) = file.read_line(&mut line) {
                    return Some(Err(format!("unable to read {}: {}", self.path.display(), e)));
                }
                if line.is_empty() {
                    return None;
                }
                self.line_number += 1;
                match serde_json::from_str(&line) {
                    Ok(event) => Some(Ok(event)),
                    Err(e) => Some(Err(format!("unable to parse line {} of {}: {}", self.line_number, self.path.display(), e))),
                }
            }
            None => None,
        }
    }
}

fn read_events() -> Result<Events, String> {
    let project_dirs = ProjectDirs::from("org", "ftbfs", "odo").ok_or_else(|| "unable to determine project directories".to_string())?;
    let path = project_dirs.data_dir().join("log");
    if path.exists() {
        let file = File::open(&path).map_err(|e| format!("unable to open {}: {}", path.display(), e))?;
        let file = BufReader::new(file);
        Ok(Events { path, line_number: 0, file: Some(file) })
    } else {
        Ok(Events { path, line_number: 0, file: None })
    }
}

struct Task {
    uuid: Uuid,
    description: String,
}

fn read_tasks() -> Result<Vec<Task>, String> {
    let mut map = BTreeMap::new();
    for event in read_events()? {
        match event? {
            Event::Add { uuid, description, .. } => {
                map.insert(uuid, description);
            }
            Event::Do { uuid, .. } => {
                map.remove(&uuid);
            }
            Event::Edit { uuid, description, .. } => {
                map.insert(uuid, description);
            }
            Event::Remove { uuid, .. } => {
                map.remove(&uuid);
            }
        }
    }
    let mut tasks = Vec::new();
    for (uuid, description) in map {
        tasks.push(Task { uuid, description });
    }
    Ok(tasks)
}
