# odo(1) completion                                    -*- shell-script -*-

# Copyright 2018-2019 Matt Kraai

# This file is part of odo.

# odo is free software: you can redistribute it and/or modify it under the terms
# of the GNU Affero General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# odo is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License along
# with odo.  If not, see <https://www.gnu.org/licenses/>.

_odo()
{
    local cur prev words cword
    _init_completion || return

    # see if the user selected a command already
    local COMMANDS=(
        "add"
	"count"
        "do"
	"edit"
	"grep"
	"log"
        "rm")

    local command i
    for (( i=0; i < ${#words[@]}-1; i++ )); do
        if [[ ${COMMANDS[@]} =~ ${words[i]} ]]; then
            command=${words[i]}
            break
        fi
    done

    # supported options per command
    if [[ "$cur" == -* ]]; then
        case $command in
	    add)
                COMPREPLY=( $( compgen -W '--done --quiet -q --unique -u' -- "$cur" ) )
                return 0
                ;;
            grep)
                COMPREPLY=( $( compgen -W '--ignore-case -i' -- "$cur" ) )
                return 0
                ;;
	esac
    fi

    # specific command arguments
    if [[ -n $command ]]; then
        case $command in
            do|edit|rm)
	        COMPREPLY=( $( odo | cut -c-36 | grep "^$cur" ) )
                return 0
                ;;
        esac
    fi

    # no command yet, show what commands we have
    if [ "$command" = "" ]; then
        COMPREPLY=( $( compgen -W '${COMMANDS[@]}' -- "$cur" ) )
    fi

    return 0
} &&
complete -F _odo odo

# ex: ts=4 sw=4 et filetype=sh
