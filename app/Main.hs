{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad
import Data.List
import Database.SQLite.Simple
import Options.Applicative
import System.Directory
import System.FilePath
import System.IO

main :: IO ()
main = do
  opts <- execParser opts
  homeDirectory <- getHomeDirectory
  let directory = homeDirectory </> "Library/Application Support/odo"
  createDirectoryIfMissing True directory
  withConnection (directory </> "odo.sqlite") $ \conn -> do
    execute_
      conn
      "CREATE TABLE IF NOT EXISTS goal (description TEXT PRIMARY KEY)"
    case opts of
      Add description -> do
        execute conn "INSERT INTO goal VALUES(?)" (Only description)
      Do description -> do
        execute conn "DELETE FROM goal WHERE description = ?" (Only description)
        c <- changes conn
        when (c /= 1) $
          hPutStrLn stderr ("odo: \"" ++ description ++ "\" does not exist")
      List -> do
        rows <- query_ conn "SELECT * FROM goal"
        forM_ rows $ \(Only description) -> putStrLn description

data Opts
  = Add String
  | Do String
  | List
  deriving (Show)

opts :: ParserInfo Opts
opts =
  info
    (helper <*>
     hsubparser
       (command
          "add"
          (info
             (Add . unwords <$> some (argument str (metavar "DESCRIPTION...")))
             (progDesc "Add a goal")) <>
        command
          "do"
          (info
             (Do . unwords <$> some (argument str (metavar "DESCRIPTION...")))
             (progDesc "Do a goal")) <>
        command "ls" (info (pure List) (progDesc "List goals"))))
    (progDesc "Manage goals")
